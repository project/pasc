CONTENTS OF THIS FILE
---------------------

 * Introduction
 * Installation


INTRODUCTION
------------

The Performance and Scalability Checklist is an interactive checklist of
performance and scalability optimization tasks for Drupal. It will help you
optimize the software stack your web site operates on, from Apache up to your
Drupal theme.


INSTALLATION
------------

This module is installed in the usual way. See
https://www.drupal.org/docs/user_guide/en/extend-module-install.html.
